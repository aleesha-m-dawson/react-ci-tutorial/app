# React CI Tutorial
This project is used to demonstart how to use GitLab CI/CD features. The React Application was inspired by [this application](https://github.com/nordicgiant2/react-nice-resume) found on GitHub.

## Dependencies
This application uses 
- [`Node.js`](https://nodejs.org/en/about/) (v16.0.0+)
- [`npm`](https://www.npmjs.com/) (v9.2.0+). 

Optionally, this appication uses: 
- [`Docker`](https://www.docker.com/) (20.10.17+).

## Run Appliction
To run the application locally, you must first clone the repository:

<!-- TODO: Add git clone URL -->

**NOTE:** If you created this project from a template or a fork, replace the above clone URL with your project URL

We can run the application natively on your machine or via `Docker`. 

To run the application natively, use the following commands:
```{shell}
npm install
npm run build
npm start
```

To run the application in a container, use the following commands:
```{shell}
docker build . -t react-ci-tutorial:test-image
docker run -p 3000:80 -d --name test react-ci-tutorial:test-image
```

Visit `localhost:3000` to see the running application.

## Run Tests
To run the unit tests locally, run the following:
```
npm test
```

## Troubleshooting

1. To clone the repository over HTTPS, use a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
